package com.sccl.iottm.dao;

import com.sccl.iottm.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @Author Dave
 * @Date 2018/9/6
 * @Description
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    /**
     * @return
     */
    User findByNameAndPassword(String name, String password);

    User findUserByName(String name);
}
