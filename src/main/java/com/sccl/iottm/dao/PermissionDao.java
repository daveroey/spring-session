package com.sccl.iottm.dao;

import com.sccl.iottm.entity.Permission;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author Dave
 * @Date 2018/10/11
 * @Description
 */
public interface PermissionDao extends JpaRepository<Permission, Long> {

    @Query(value = "SELECT P.* FROM permission AS P\n" +
            "JOIN perssion_role AS PR\n" +
            "ON PR.perssion_id=P.id\n" +
            "JOIN role_user as R\n" +
            "ON R.role_id=PR.role_id\n" +
            "WHERE R.user_id=?", nativeQuery = true)
    List<Permission> findPermissionByRoleId(Long id);
}
