package com.sccl.iottm.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * @Author Dave
 * @Date 2018/10/12
 * @Description
 */
@Component
public class MyPasswordEncoder implements PasswordEncoder {
    private final Logger log=LoggerFactory.getLogger(MyPasswordEncoder.class);
    @Override
    public String encode(CharSequence charSequence) {
        log.info("加密字符串：{}",charSequence.toString());
        return charSequence.toString();
    }

    @Override
    public boolean matches(CharSequence charSequence, String s) {
        log.info("密码：{},数据库：{}",charSequence.toString(),s);
        return s.equals(charSequence.toString());
    }
}
