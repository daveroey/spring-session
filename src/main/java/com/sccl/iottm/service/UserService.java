package com.sccl.iottm.service;

import com.sccl.iottm.entity.User;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * @Author Dave
 * @Date 2018/9/6
 * @Description
 */
public interface UserService extends UserDetailsService {

    /**
     *
     * @param user
     */
    void addUser(User user);

    User findUser(String name,String pwd);

    User findUserByName(String name);

}
