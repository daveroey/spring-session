package com.sccl.iottm.action;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author Dave
 * @Date 2018/10/13
 * @Description
 */
@RestController
@RequestMapping("/config/*")
public class ConfigAction {

    @RequestMapping(value = "/sessionInvalid")
    public String sessionInvalid() {
        return "session 过期了。。。";
    }
}
