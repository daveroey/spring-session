package com.sccl.iottm.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

/**
 * @Author Dave
 * @Date 2018/10/13
 * @Description
 */
//httpsession将有spring-session支持
@Configuration
@EnableRedisHttpSession
public class SessionConfig {

}
